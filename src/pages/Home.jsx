import React from 'react'
import Greeting from '../component/Greeting'
import Lampu from '../component/Lampu'

class Home extends React.Component {
    render() {
        return (
            <div className="container">
                <Greeting isLogin={true} />
                <Lampu />

            </div>
        )
    }
}

export default Home