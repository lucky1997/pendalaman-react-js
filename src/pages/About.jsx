import React from 'react'
import Greeting from '../component/Greeting'
import Lampu from '../component/Lampu'
import Warna from '../component/Warna'
import Table from '../component/Table'

class About extends React.Component {
    render() {
        return (
            <div className="container">
                <Warna />
                <Table />

            </div>
        )
    }
}

export default About