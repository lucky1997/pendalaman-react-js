import React from 'react'

class Lampu extends React.Component {
    constructor(props) {
        super(props)
        this.state = {isLampOn: true}
        this.saklarKlik = this.saklarKlik.bind(this)
    }

    saklarKlik() {
        this.setState(state=>({
            isLampnOn: !state.isLampOn
        }))
    }

    render(){
        return(
            <button className="btn btn-primary" onclick={this.saklarKlik}>
                {this.state.isLampOn ? 'Hidup': 'Mati'}
            </button>
        )
    }
}

export default Lampu