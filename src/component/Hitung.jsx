import React, { useEffect, useState } from 'react'

function Hitung (){
    const[hitung, setHitung] = useState(0)

    useEffect(() => {
        document.title = 'Hitung ke- ${hitung}'
    }) 
    return (
        <div className="container">
            <p>Kamu sudah mengklik hitung sebnayak {hitung}</p>
            <button className="btn btn-success" onClick={()=>setHitung(hitung+1)}>Klik</button>
        </div>
    )
}

export default Hitung