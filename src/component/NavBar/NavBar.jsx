import React from 'react'
import { Navbar, Nav, Form, FormControl, Button } from 'react-bootstrap'

class NavBar extends React.Component {
    render() {
        return(
            <div>
                    <Navbar bg="dark" variant="dark">
                    <Navbar.Brand href="#home">Navbar</Navbar.Brand>
                    <Nav className="mr-auto">
                        <Nav.Link href="/">Home</Nav.Link>
                        <Nav.Link href="/barang">Barang</Nav.Link>
                        {/* <Nav.Link href="/about">About</Nav.Link>
                        <Nav.Link href="/hitung">Hitung</Nav.Link> */}
                    </Nav>
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-info">Search</Button>
                    </Form>
                </Navbar>
            </div>
        )
    }
}

export default NavBar