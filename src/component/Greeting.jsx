import React from 'react'

function GiveGreet() {
    return(
        <h1>Selamat Datang</h1>
    )
}

function Regret() {
    return(
        <h1>Maaf Belum Login</h1>
    )
}

function Greetings(props) {
    const isLogin = props.isLogin
    if(isLogin) {
        return <GiveGreet />
    }
    return <Regret />
}

export default Greetings