import {BrowserRouter, Route} from 'react-router-dom'
import NavBar from './component/NavBar/NavBar'
import Lampu from './component/Lampu'
import Table from './component/Table'
import React from 'react'
import About from './pages/About'
import Home from './pages/Home'
import Hitung from './component/Hitung'
import Home2 from './TokoOnline/Home'
import Barang from './TokoOnline/Barang'

function Router () {
    return (
        <BrowserRouter>
            <NavBar/>
                <Route exact path="/" component={Home2} />
                <Route exact path="/barang" component={Barang} />
            {/* <Route exact path="/" component={Home} />
            <Route exact path="/table" component={Table} />
            <Route exact path="/about" component={About} />
            <Route exact path="/hitung" component={Hitung} /> */}
        </BrowserRouter>
    )
}

export default Router