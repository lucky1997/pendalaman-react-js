import React from 'react'
import Login from './Login'

class Home extends React.Component {
    render() {
        return (
            <div className="container">
                <Login isLogin={false} />
            </div>
        )
    }
}

export default Home