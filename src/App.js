import React from 'react';
import logo from './logo.svg';
import './App.css';
import NavBar from './component/NavBar/NavBar';
import Warna from './component/Warna';
import Lampu from './component/Lampu';
import Greetings from './component/Greeting';
import Table from './component/Table';

function App() {
  return (
    <div className="App">
      <NavBar />
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
        <Lampu />
        <Warna />
        <br />
        <Greetings />
        <br />
        <Table />
      </header>
    </div>
  );
}

export default App;
